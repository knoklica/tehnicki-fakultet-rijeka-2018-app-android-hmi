package com.intelup.smsmobile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.BundleCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Mihovil on 11.8.2017..
 */

public class LoginClass extends AppCompatActivity {

    private boolean doubleBackToExitPressedOnce;

    private String[] users = {"Administrator", "Operator",""};
    private String[] pass = {"Administrator", "1234",""};
    private String[] privilage = {"Admin", "Demo","Admin"};

    @Override
    protected void onCreate(Bundle bundle){
        super.onCreate(bundle);

        setContentView(R.layout.login_activity);

    }


    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.back_message), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    public void loginPress(View view){
        EditText username = (EditText) findViewById(R.id.username);
        EditText password = (EditText) findViewById(R.id.password);
        TextInputLayout userTIL = (TextInputLayout) findViewById(R.id.userTIL);
        TextInputLayout passTIL = (TextInputLayout) findViewById(R.id.passTIL);

        passTIL.setError(null);

        for (int i = 0; i<users.length;i++){
            if (users[i].equals(username.getText().toString())) {
                userTIL.setError(null);
                if (pass[i].equals(password.getText().toString())){

                    Toast.makeText(this, getString(R.string.login_succ), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginClass.this, Mainmenu.class);
                    intent.putExtra("privilage", privilage[i]);
                    startActivity(intent);
                } else {
                    passTIL.setError(getResources().getString(R.string.passinv));
                }
                break;
            } else {
                userTIL.setError(getResources().getString(R.string.userinv));
            }
        }


    }


}
