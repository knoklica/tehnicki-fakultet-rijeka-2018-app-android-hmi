package com.intelup.smsmobile;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mihovil on 12.8.2017..
 */

public class Mainmenu extends AppCompatActivity{

    private Handler mHandler = new Handler();
    private int poolRate = 500;
    private String cookie;


    private TextView motorSTOP;
    private TextView motorVEL;
    private TextView bazenLVL;


    private TextInputEditText motorVELIN;
    private Switch motorONS;
    private String onpom;

    @Override
    protected void onCreate(Bundle bundle){
        super.onCreate(bundle);

        setContentView(R.layout.mainmenu);


        motorSTOP = (TextView) findViewById(R.id.motorstop);
        motorVEL = (TextView) findViewById(R.id.motorvelout);
        bazenLVL = (TextView) findViewById(R.id.bazenlvl);

        motorVELIN = (TextInputEditText) findViewById(R.id.motorvelin);
        motorONS = (Switch) findViewById(R.id.motoron);

        initGETMethod("http://192.168.1.1/awp/Testing/test.htm");
        //getCookie("http://192.168.1.1/Portal/Intro.mwsl");

        startPooling();



        FloatingActionButton FAB = (FloatingActionButton) findViewById(R.id.floatbutton);
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  initPOSTMethod("http://192.168.1.1/awp/Testing/test2.htm?\"androiddata\".val=124");
                initPOSTMethod("http://192.168.1.1/awp/Testing/test2.htm");
            }
        });

        motorONS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    onpom="1";
                    initPOSTmotorONMethod("http://192.168.1.1/awp/Testing/test2.htm");

                }
                else {
                    onpom="0";
                    initPOSTmotorONMethod("http://192.168.1.1/awp/Testing/test2.htm");
                }
            }
        });
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        stopPooling();
    }

    Runnable poolVals = new Runnable() {
        @Override
        public void run() {
            try{
                initGETMethod("http://192.168.1.1/awp/Testing/test.htm");
            } finally {
                mHandler.postDelayed(poolVals, poolRate );
            }
        }
    };

    private void startPooling(){
        poolVals.run();
    }

    public void stopPooling(){
        mHandler.removeCallbacks(poolVals);
    }

    public void initGETMethod(String url) {

       // swipeRefreshLayout.setRefreshing(true);
       // pivaAdapterRec.clear();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        motorSTOP.setText(response.optString("MotorSTOP"));
                        motorVEL.setText(response.optString("MotorVEL"));
                        bazenLVL.setText(response.optString("BazenLVL"));

                        if (response.optString("MotorON").equals("0"))  {
                            motorONS.setChecked(false);
                        } else motorONS.setChecked(true);



                       // resolver(response);
                      //  swipeRefreshLayout.setRefreshing(false);
                      //  loading = false;

                        //Snackbar.make(swipeRefreshLayout, "Updated", Snackbar.LENGTH_LONG).show();


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();

                      //  swipeRefreshLayout.setRefreshing(false);
                      //  Snackbar.make(swipeRefreshLayout, "Failed to update!", Snackbar.LENGTH_LONG).show();
                      //  loading = false;
                    }
                }) {


        };

        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    public void initPOSTMethod(String url) {

        // swipeRefreshLayout.setRefreshing(true);
        // pivaAdapterRec.clear();

        StringRequest postRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        // resolver(response);
                        //  swipeRefreshLayout.setRefreshing(false);
                        //  loading = false;

                        //Snackbar.make(swipeRefreshLayout, "Updated", Snackbar.LENGTH_LONG).show();


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();

                        //  swipeRefreshLayout.setRefreshing(false);
                        //  Snackbar.make(swipeRefreshLayout, "Failed to update!", Snackbar.LENGTH_LONG).show();
                        //  loading = false;
                    }
                }) {
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("\"Motor_Pump_DB\".MotorVELREF", motorVELIN.getText().toString());
              //  params.put("\"androiddata\".val", "666");
            return params;
            };


        };

        MySingleton.getInstance(this).addToRequestQueue(postRequest);
    }


    public void initPOSTmotorONMethod(String url) {

        // swipeRefreshLayout.setRefreshing(true);
        // pivaAdapterRec.clear();

        StringRequest postRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        // resolver(response);
                        //  swipeRefreshLayout.setRefreshing(false);
                        //  loading = false;

                        //Snackbar.make(swipeRefreshLayout, "Updated", Snackbar.LENGTH_LONG).show();


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();

                        //  swipeRefreshLayout.setRefreshing(false);
                        //  Snackbar.make(swipeRefreshLayout, "Failed to update!", Snackbar.LENGTH_LONG).show();
                        //  loading = false;
                    }
                }) {
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("\"Motor_Pump_DB\".MotorON", onpom);
                //  params.put("\"androiddata\".val", "666");
                return params;
            };


        };

        MySingleton.getInstance(this).addToRequestQueue(postRequest);
    }


    public void RunPOSTMethod(View view) {

        // swipeRefreshLayout.setRefreshing(true);
        // pivaAdapterRec.clear();
        String url = "http://192.168.1.1/ClientArea/CPUAction.mwsl?Action=Start";

        StringRequest postRequest = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        // resolver(response);
                        //  swipeRefreshLayout.setRefreshing(false);
                        //  loading = false;

                        //Snackbar.make(swipeRefreshLayout, "Updated", Snackbar.LENGTH_LONG).show();


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();

                        //  swipeRefreshLayout.setRefreshing(false);
                        //  Snackbar.make(swipeRefreshLayout, "Failed to update!", Snackbar.LENGTH_LONG).show();
                        //  loading = false;
                    }
                }) {
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Referer", "http://192.168.1.1/Portal/Portal.mwsl?PriNav=Start");
                //  params.put("\"androiddata\".val", "666");
                return headers;
            };

        };

        MySingleton.getInstance(this).addToRequestQueue(postRequest);
    }

    public void StopPOSTMethod(View view) {

        // swipeRefreshLayout.setRefreshing(true);
        // pivaAdapterRec.clear();
        String url = "http://192.168.1.1/ClientArea/CPUAction.mwsl?Action=Stop";

        StringRequest postRequest = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        // resolver(response);
                        //  swipeRefreshLayout.setRefreshing(false);
                        //  loading = false;

                        //Snackbar.make(swipeRefreshLayout, "Updated", Snackbar.LENGTH_LONG).show();


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();

                        //  swipeRefreshLayout.setRefreshing(false);
                        //  Snackbar.make(swipeRefreshLayout, "Failed to update!", Snackbar.LENGTH_LONG).show();
                        //  loading = false;
                    }
                }) {
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Referer", "http://192.168.1.1/Portal/Portal.mwsl?PriNav=Start");
                //  params.put("\"androiddata\".val", "666");
                return headers;
            };


        };

        MySingleton.getInstance(this).addToRequestQueue(postRequest);
    }

    public void FlashPOSTMethod(View view) {

        // swipeRefreshLayout.setRefreshing(true);
        // pivaAdapterRec.clear();
        String url = "http://192.168.1.1/ClientArea/CPUAction.mwsl?Action=FlashLeds";

        StringRequest postRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        // resolver(response);
                        //  swipeRefreshLayout.setRefreshing(false);
                        //  loading = false;

                        //Snackbar.make(swipeRefreshLayout, "Updated", Snackbar.LENGTH_LONG).show();


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();

                        //  swipeRefreshLayout.setRefreshing(false);
                        //  Snackbar.make(swipeRefreshLayout, "Failed to update!", Snackbar.LENGTH_LONG).show();
                        //  loading = false;
                    }
                }) {
            @Override
            public Map<String, String> getHeaders(){
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Referer", "http://192.168.1.1/Portal/Portal.mwsl?PriNav=Start");
                //  params.put("\"androiddata\".val", "666");
                return headers;
            };



        };

        MySingleton.getInstance(this).addToRequestQueue(postRequest);
    }


   /* public void getCookie(String url) {



        StringRequest req = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("response",response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }){

          /*  @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getAuthHeader(context);
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                // since we don't know which of the two underlying network vehicles
                // will Volley use, we have to handle and store session cookies manually
                Log.i("response",response.headers.toString());
                Map<String, String> responseHeaders = response.headers;
                String rawCookies = responseHeaders.get("Set-Cookie");
                Log.i("cookies",rawCookies);

                cookie=rawCookies;

                return super.parseNetworkResponse(response);
            }

        };

        MySingleton.getInstance(this).addToRequestQueue(req);
    }*/

}
